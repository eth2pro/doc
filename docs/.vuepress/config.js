module.exports = {
    head: [
        ['link', { rel: 'icon', href: '/eth-home-icon.png' }]
    ],
    title: 'Ethereum To Professional',
    description: 'Ethereum To Professional',
    base: '/doc/',
    dest: 'public',
    themeConfig: {
        sidebar: { '/': getGuideSidebar('参与2.0验证', '2.0 WiKi', '智能合约','Golang', '更多') },
        // 假定是 GitHub. 同时也可以是一个完整的 GitLab URL
        repo: 'https://gitlab.com/eth2pro/doc',
        // 自定义仓库链接文字。默认从 `themeConfig.repo` 中自动推断为
        // "GitHub"/"GitLab"/"Bitbucket" 其中之一，或是 "Source"。
        repoLabel: 'GitLab'
    }
}

function getGuideSidebar(group1, group2, group3, group4,group999) {
    return [
        {
            title: group1,
            collapsable: false,
            children: [
                '',
                'how-deposit',
                'start-validator'
            ]
        },
        {
            title: group2,
            collapsable: false,
            children: [
                'glossary'
            ]
        }, {
            title: group3,
            collapsable: false,
            children: [
                'dapp-brief',
                'whatisdapp',
                'auction-dapp-example',
                'contract'
            ]
        }, {
            title: group4,
            collapsable: false,
            children: [
                'golang-easy-to-err'
            ]
        },{
            title: group999,
            collapsable: false,
            children: [
                'related-links',
                'about-us'
            ]
        }
    ]
}