# 相关链接

## ETH2.0相关
* [ETH2.0主网LaunchPad](https://launchpad.ethereum.org)
* [ETH2.0测试网LaunchPad](https://pyrmont.launchpad.ethereum.org/)
* [ETH2.0主网信标链浏览器](https://beaconcha.in/)
* [ETH2.0测试网信标链浏览器](https://pyrmont.beaconcha.in/)

## 以太坊Developer相关
* [Solidity语言中文文档](https://solidity-cn.readthedocs.io/zh/develop/index.html)
* [Truffle文档](https://www.trufflesuite.com/docs/truffle/overview)
* [ethers文档](https://docs.ethers.io/v5/)
* [web3文档](https://web3js.readthedocs.io/en/v1.2.9/)
* [ethereumjs-abi](https://github.com/ethereumjs/ethereumjs-abi)