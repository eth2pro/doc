# 启动验证程序

## 1. 下载Teku

从[github](https://github.com/ConsenSys/teku)上下载Teku软件包，或者直接从源码编译都可以。从源码编译的话，你需要先clone代码到本地，然后进入到Teku目录，运行gradlew文件进行编译。编译好之后会在build文件夹下生成编译好的程序文件。

## 2. 启动Teku

进入到软件包所在的bin目录，执行如下的teku命令，即可启动验证者客户端。
   ``` bash
   teku --network=pyrmont --eth1-endpoint=https://goerli.infura.io/v3/ce064631a5954a08b8bdfdb6d082a6ec  --validator-keys=C:/Users/Administrator/Downloads/eth2deposit-cli-ed5a6d3-windows-amd64/validator_keys/keystore-m_12381_3600_0_0_0-1606463886.json;C:/Users/Administrator/Downloads/eth2deposit-cli-ed5a6d3-windows-amd64/validator_keys/keystore-m_12381_3600_0_0_0-1606463886.txt  --rest-api-enabled=true --rest-api-docs-enabled=true  --metrics-enabled
   ```
成功启动程序后，会显示如下所示的界面。

![](./images/eth2deposit-cli-9.png)

这里简单解析下Teku启动命令各项的含义：

* --network表示你连接的网络，上面命令连接的是pyrmont测试网。大家使用的时候测试网文件和主网建议分开。
* --eth1-endpoint表示连接的1.0网络的数据源。
* --validator-keys表示key文件和密码的路径，中间用分号分开。
* --rest-api-enabled表示开启rest api。
* --rest-api-docs-enabled开启rest api文档。
* --metrics-enabled开启各项指标，这样你可以通过[eth2stats](https://eth2stats.io/add-node)去监控你的程序状态。

