# 简介

去中心化APP-Decentralized Applications (DApps)，在本节中我们主要会学习到以下内容：

* 什么是DAPP，它有什么样的特点，由哪些部分组成
* 如何开发和部署Dapp
* Swarm和ENS