# 关于

## 简介

Eth2Pro是一个研究以太坊及相关技术的开源文档项目，采用vuepress开发，目前有以下内容：

* 关于2.0节点运行的端到端指南，我们以[Teku](https://github.com/ConsenSys/teku)为例，对如何搭建运行以太坊2.0的节点做了详细的介绍。
* 以太坊2.0词汇表
* 以太坊2.0资源链接
* 以太坊DAPP开发

对于后续会增加的内容，根据大家的需求和兴趣爱好来，我们并没有范围限制，但总体上来说聚焦于区块链相关技术。

你可以[点击此处](https://eth2pro.gitlab.io/doc/)来查看文档的全部内容。如果发现任何错误，欢迎给我们提[Issues](https://gitlab.com/eth2pro/doc/-/issues)。

## 加入讨论组

你可以添加作者微信`easydefi`(请备注`eth2pro`)加入微信交流群，和我们一起交流相关技术问题。

## 贡献

我们非常欢迎任何对相关技术感兴趣的朋友参与贡献，只需要以下简单的步骤，就可以在eth2pro上来展示你的文档。

* 首先clone一份我们的master分支到你的本地，你只需要用npm install vuepress，便可以在本地将项目运行起来

* 然后在你本地新建一个分支，以你的 `feature/增加的主题` 来给该分支命名，然后切换到该分支下添加你要写的内容

* 添加内容时候，先在config.js-getGuideSidebar增加相应的group，然后在docs下增加相应的md文件即可

* 当在本地运行达到你预期的效果后，提交该分支到gitlab，并提交一个Pull Request即可。

* 当其他伙伴帮忙检视没有问题后会merge到master分支，然后就可以在eth2.pro上展现你的成果了。

## 更多参考

* [vuepress](https://vuepress.vuejs.org/)
* [markdown](https://www.markdownguide.org/)