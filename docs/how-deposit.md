# 如何进行抵押？

抵押的具体操作在[Launchpad](https://pyrmont.launchpad.ethereum.org/)上有详细的提示，根据提示一步步操作非常简单。下面我们阐述一下主要的步骤：

## [知情同意](https://pyrmont.launchpad.ethereum.org/overview)

在你点击`GET STARTED`后会进入抵押操作的知情同意部分，主要阐述了以下注意事项：
* 成为验证者你必须抵押32个以太币到1.0的主网，并且这一操作是不可逆的，也就是你一旦抵押了便不可撤回。
* 做为验证者，你必须保证验证程序时刻在线，离线后会接受惩罚。
* 你需要保管好你的助记词，这个是你将来提取你以太币的唯一方式。
* 直到阶段2你才可以提取你的以太币，以太坊大概需要2年左右才能完成阶段2。
* 在阶段2之前你无法主动推出验证者。也就是说现阶段做为验证者一旦加入无法退出。
* 由于以太坊2.0是新的软件可能会有一些潜在未被发现的bug可能会给你造成资金上的损失。

## [选择客户端](https://pyrmont.launchpad.ethereum.org/select-client)

1. 选择1.0的客户端，并搭建1.0的节点。

为什么要搭建1.0的节点，在[上一节](/#运行验证程序的步骤)我们也有进行阐述。这里我们不做重复介绍啦。

2. 选择2.0的客户端。

在我们的文档中主要是以[Teku](https://github.com/ConsenSys/teku)为例进行的介绍，其他客户端操作请参考相关的文档。

## [生成Key](https://pyrmont.launchpad.ethereum.org/generate-keys)

在这里步主要是生成你抵押，以及后面启动验证程序所需的key文件。key文件可以通过相关的工具来生成，[Launchpad](https://pyrmont.launchpad.ethereum.org/generate-keys)提供了各个不同平台的key生成程序。我们以windows为例讲解一下如何使用。

你可以直接[点击此处](https://github.com/ethereum/eth2.0-deposit-cli/releases/),在打开的页面点击`eth2deposit-cli-ed5a6d3-windows-amd64.zip`下载。下载完成后解压到任一目录，会有一个`deposit.exe`的程序。

然后打开Windwos的命令行界面，并定位到`deposit.exe`程序当前所在的目录，然后输入以下命令：

``` bash
deposit.exe new-mnemonic --chain pyrmont
``` 

接下来，会提示你选择助记词的语言，这里默认使用英语，所以建议直接回车进入下一步。

![](./images/eth2deposit-cli-1.png)

这一步，会让你输入，需要运行多少个验证者，每个验证者需要抵押32个以太币，你需要运行多少个就填多少，我们以1个为例。输入1即可。

![](./images/eth2deposit-cli-2.png)

在这一步，会要你输入密码来加密你的keystore文件。输入完回车再确认一次即可。

![](./images/eth2deposit-cli-3.png)

接下来，确认你的助记词，将屏幕上显示的助记词抄写在安全的地方，请一定要抄写下来再回车进入下一步。

![](./images/eth2deposit-cli-4.png)

将你刚刚抄写的助记词再输入一遍，来确认是否正确。最后生成下图这样一个界面即表示你的key生成成功了。你可以去相应的路径下面找到生成的key。

![](./images/eth2deposit-cli-5.png)

## [上传deposit_data.json文件](https://pyrmont.launchpad.ethereum.org/upload-validator)

在[LaunchPad](https://pyrmont.launchpad.ethereum.org/upload-validator)中如下图所示位置上传你上一步生成的deposit_data开头的json文件。

![](./images/eth2deposit-cli-6.png)

## [连接钱包发送抵押](https://pyrmont.launchpad.ethereum.org/connect-wallet)

接下来就是连接你的metamask钱包，勾选所有的确认事项，然后发送32个以太币到抵押合约完成抵押。

![](./images/eth2deposit-cli-8.png)

至此，你的抵押操作就完成了，接下来可以启动Teku验证程序，激活验证者。
