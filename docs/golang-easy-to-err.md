# Golang中易犯的错误

### 测试中不使用race detector

数据竞争引发的错误经常发生在我们线上代码部署很长一段时间后。为了帮助提前检测代码中是否有这类bug.从go1.1开始就引入了一个内置的数据竞争检测器。使用时只需简单加一个 `-race` 标志即可，例如：
 * go test -race pke
 * go run -race main.go
 * go build -race