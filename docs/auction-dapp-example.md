# Auction Dapp Example


这个拍卖的Dapp允许用户注册 Deed Token来代表某一特定资产，例如：一套房子，一辆汽车，一个商标，等等。创建Token的时候会注册其Owner，然后系统中会列出所有注册的资产，来允许其他用户来竞标，在每一轮竞标的过程中，用户可以加入一个为该拍卖创建的room.一旦拍卖结束，相应的Deed token的owner会转移给新中标的人。整个流程可以通过下图来理解：

![avator](https://github.com/ethereumbook/ethereumbook/raw/develop/images/auction_diagram.png)

那么，实现这样的一个去中心化应用有哪些组成部分呢？
* 一个实现ERC721非同质化代币的合约(DeedRepository)
* 一个实现出售Deed的拍卖合约(AuctionRepository)
* web 前端，也就是用户界面
* web3.js库来链接以太坊区块链
* Swarm client ，来存储图片资源
* Whisper client，为参与者创建拍卖的聊天室

接下来我们来具体看下backend smart contract，首先我们看下DeedRepository合约

```javascript

pragma solidity ^0.5.16;
import "./ERC721/ERC721Token.sol";

/**
 * @title Repository of ERC721 Deeds
 * This contract contains the list of deeds registered by users.
 * This is a demo to show how tokens (deeds) can be minted and added 
 * to the repository.
 */
contract DeedRepository is ERC721Token {


    /**
    * @dev Created a DeedRepository with a name and symbol
    * @param _name string represents the name of the repository
    * @param _symbol string represents the symbol of the repository
    */
    constructor(string memory _name, string memory _symbol) 
        public ERC721Token(_name, _symbol) {}
    
    /**
    * @dev Public function to register a new deed
    * @dev Call the ERC721Token minter
    * @param _tokenId uint256 represents a specific deed
    * @param _uri string containing metadata/uri
    */
    function registerDeed(uint256 _tokenId, string memory _uri) public {
        _mint(msg.sender, _tokenId);
        addDeedMetadata(_tokenId, _uri);
        emit DeedRegistered(msg.sender, _tokenId);
    }

    /**
    * @dev Public function to add metadata to a deed
    * @param _tokenId represents a specific deed
    * @param _uri text which describes the characteristics of a given deed
    * @return whether the deed metadata was added to the repository
    */
    function addDeedMetadata(uint256 _tokenId, string memory _uri) public returns(bool){
        _setTokenURI(_tokenId, _uri);
        return true;
    }

    /**
    * @dev Event is triggered if deed/token is registered
    * @param _by address of the registrar
    * @param _tokenId uint256 represents a specific deed
    */
    event DeedRegistered(address _by, uint256 _tokenId);
}


```
该合约比较简单，就是实现了标准的ERC721Token，没什么特别的。下面看看拍卖合约AuctionRepository

```javascript

contract AuctionRepository {

    // Array with all auctions
    Auction[] public auctions;

    // Mapping from auction index to user bids
    mapping(uint256 => Bid[]) public auctionBids;

    // Mapping from owner to a list of owned auctions
    mapping(address => uint[]) public auctionOwner;

    // Bid struct to hold bidder and amount
    struct Bid {
        address from;
        uint256 amount;
    }

    // Auction struct which holds all the required info
    struct Auction {
        string name;
        uint256 blockDeadline;
        uint256 startPrice;
        string metadata;
        uint256 deedId;
        address deedRepositoryAddress;
        address owner;
        bool active;
        bool finalized;
    }

```

该合约定义了以上和拍卖相关的业务数据，和以下操作方法

```javascript

getCount()
getBidsCount(uint _auctionId)
getAuctionsOf(address _owner)
getCurrentBid(uint _auctionId)
getAuctionsCountOfOwner(address _owner)
getAuctionById(uint _auctionId)
createAuction(address _deedRepositoryAddress, uint256 _deedId,
              string _auctionTitle, string _metadata, uint256 _startPrice,
              uint _blockDeadline)
approveAndTransfer(address _from, address _to, address _deedRepositoryAddress,
                   uint256 _deedId)
cancelAuction(uint _auctionId)
finalizeAuction(uint _auctionId)
bidOnAuction(uint _auctionId)

```

接下来，我们可以在以太坊测试网ropsten上发布以上合约代码。我这里有在rospten上面发布过的，大家也可以直接使用以下地址：

* DeedRepository 0xc5E7755a3Ea1f3D80307072029a61B1c4C177bcb
* AuctionRepository 0x037AFA1b9Bf85c9b57604262eaFA0Be9903F74A1

接下来，我们来构建我们的前段代码，把网站跑起来。编译好了后会生成如下所示的文件：
```
dist/
|-- index.html
`-- static
    |-- css
    |   |-- app.0e50d6a1d2b1ed4daa03d306ced779cc.css
    |   `-- app.0e50d6a1d2b1ed4daa03d306ced779cc.css.map
    `-- js
        |-- app.5396ead17892922422d4.js
        |-- app.5396ead17892922422d4.js.map
        |-- manifest.87447dd4f5e60a5f9652.js
        |-- manifest.87447dd4f5e60a5f9652.js.map
        |-- vendor.77913f316aaf102cec11.js
        `-- vendor.77913f316aaf102cec11.js.map
```
我们将其上传到swarm上会获得该目录的hash，类似这样的值：
```
ab164cf37dc10647e43a233486cdeffa8334b026e32a480dd9cbd020c12d4581
```
然后我们在浏览器可以通过类似这样的网址进行访问：https://swarm-gateways.net/bzz:/ab164cf37dc10647e43a233486cdeffa8334b026e32a480dd9cbd020c12d4581/

至此，我们进一步将前端文件去中心化啦。但是上面的网址访问起来非常不方便，我们可以借助ENS来简化。接下来，我们先来了解下ENS是什么？

### Ethereum Name Service(ENS)

简单的来说，它就是一个以太坊区块链上的名称服务，由一组智能合约来提供这些服务。其主要作用是在智能合约里面建立单词和以太坊地址（当然可以不仅仅是以太坊地址，还可以是swarm hash等)之间的一一映射关系，这样用户就可以通过输入有意义的单词来给对方转以太币（访问智能合约，和swarm上的文件）。例如：在以太坊钱包里面输入ethereum.eth即可给[以太坊基金会](https://etherscan.io/address/0xfB6916095ca1df60bB79Ce92cE3Ea74c37c5d359)的地址转账。

这有点类似于在中心化世界，我们将域名映射成ip地址的功能，所以我们也将ENS称为以太坊域名。只不过ENS这一套是部署在以太坊区块链上的，因此是去中心化的。接下来我们来看下ENS的实现原理。

整个ENS服务从下往上可以分为三层，底层管理者域名的Owner和Resolvers，中间层管理着名称节点，上层管理着锁定的资金。我们一个个来看一下：

#### Bottom Layer: Name Owners and Resolvers

ENS操作的是节点，而不是名称本身。名称通过namehash（通过递归算法可以将任意有效的输入转换成唯一的固定长度的输出）算法来转换成节点。基础层的功能很简单，其作用只是允许节点的owner设置名称和创建子节点。也只有节点的owner可以设置节点的信息和创建子节点，接下来我们来看看ENS里面根节点的权限是如何设计的。

##### Root node ownership

ENS的终极目标是为新节点创建一个去中心化的决策机制。而当前写入根节点由多签机制来控制，拥有权限的人来自不同的国家，要做出一项更改，只要7个人中有4个签名即可生效。

目前这些key holder和社区一起致力于以下目标：
* .eth顶级域名合约的迁移和升级
* 社区一致同意后增加新的顶级域名
* 迁移根域名多签权限到更加去中心化的合约
* 解决顶级注册表的bug和漏洞

接下来我们看看Resolvers

##### Resolvers

因为ENS的基础合约无法增加元数据，因此需要解析合约来做这些工作。这些合约是由用户自己创建的，它能够知道相应name的信息，例如：该name对应的 Swarm address 是多少，该name对应的收款地址是多少，等等。

#### Middle Layer: The .eth Nodes

中间层，当前只有eth节点。以.eth结尾的域名可以通过[拍卖系统](https://app.ens.domains/)获取，开发者没有任何保留和优先权。在传统的拍卖会中，每一个投标人需要提交一个密封的标的，所有人同时揭标，出价最高者中标。而在区块链的世界里就有些不同了：
* 首先，为了避免竞标人出没付费意愿的标，它们必须锁定大于或等于标的的资金。
* 因为链上数据的公开透明性，你没办法隐藏标的信息，因此竞标者至少需要执行两次交易，有一个提交和揭标的过程。
* 由于去中心化的无法所有人同时揭标，因此他们必须自己一个个揭标，如果不参与揭标，前面冻结的资金会被罚没。

因此整个拍卖过程分为四步：
* 注册拍卖，创建截止日期，因为注册的是名称的hash，因此只有注册人自己知道竞拍的情况。
* 投标，这个必须在截止日期前做，其实主要是发送抵押资金。
* 揭标，计算中标者。每一次揭标都会重新计算当前的winner,在截止日期前最后的一位winner便是整个竞拍的中标者。
* 最后一步，如果你中标了，退回你的差价

#### Top Layer: The Deeds

ENS的上层由一些简单合约组成，它们的目的仅仅是负责掌管注册域名的资金。当你中标后，你的资金没有被发送到其他地方而是在ENS的合约里。会被锁定至少一年时间。如果到期后owner不想再拥有这个名称，可以将名称卖回系统，取回自己的以太币。

资金管理方面，ENS并没有使用单个的合约，而是为每一个name创建一个合约来管理资金，因此该资金只能被发回给name的owner，这种分散管理避免了资金被攻击的风险。

想注册ENS的朋友可以[点击这里](https://app.ens.domains/)体验！

接下来，我们再回头来看看ENS解析器。当你成功注册你的域名后，你可以在类似如下的界面设置你的域名解析器。

![avator](https://github.com/ethereumbook/ethereumbook/raw/develop/images/ens-manager-set-default-resolver.png)

你可以使用ENS默认的解析器合约，也可以自己创建自定义的解析器，这样做的好处是你可以将你的name解析到任何你想解析的内容，比如：经纬度代表一个地理位置。

默认的解析器提供了解析swarm hash的功能，因此我们可以将auction.ethereumbook.eth解析到`bzz:/ab164cf37dc10647e43a233486cdeffa8334b026e32a480dd9cbd020c12d4581` 这样的swarm地址。这样就可以通过
```
http://swarm-gateways.net/bzz:/auction.ethereumbook.eth/
```
来访问
```
https://swarm-gateways.net/bzz:/ab164cf37dc10647e43a233486cdeffa8334b026e32a480dd9cbd020c12d4581
```
前者具有更强的可读性。

### 从 App 到 DApp
让我们来总结下如何一步步将我们的App变成Dapp。首先我们的后端通过智能合约来控制，它们运行在去中心化区块链上，没有任何中心化的操控者。接下来，将我们的前段文件存储在去中心化的存储系统swarm上，并且通过去中心化的域名系统ENS来访问。最后我们还可以利用whisper进行去中心化的聊天。经过以上步骤我们极大增加了我们应用的去中心化特征。最后我们再来看看该Dapp的架构。

![avator](https://github.com/ethereumbook/ethereumbook/raw/develop/images/auction_dapp_final_architecture.png)

正如以太坊创始人早期阐述的，去中心化的app是以太坊的顶层愿景。然后今天大部分称自己为去中心化的app,大多数并不是完全去中心化的。当前我们已经可以构建完全去中心化的app。随着技术的进一步成熟，我们越来越多的应用程序可以去中心化，从而产生更具弹性，抗审查性和免费的网络。

