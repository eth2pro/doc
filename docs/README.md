# 介绍


以太坊2.0的客户端目前主要有4种实现：
* [Teku](https://github.com/ConsenSys/teku) ，使用java语言。
* [Lighthouse](https://lighthouse.sigmaprime.io/)，使用rust语言。
* [Nimbus](https://our.status.im/tag/nimbus/)，使用nim语言。
* [Prysm](https://prysmaticlabs.com/)，使用go语言。

该文档主要阐述如何使用[Teku](https://github.com/ConsenSys/teku)来搭建以太坊2.0的验证者节点。我们会以Goerli测试网为例，完整地教你从头到尾搭建一个节点。后续文档的所有操作未特别说明，默认都是在Goerli测试网进行操作。

## 准备工作

在正式搭建一个测试网之前，你有一些准备工作要做。因为连接的是Goerli测试网，所以我们首先需要获取Goerli测试网的以太币。官方有提供测试网以太币的水龙头，你可以打开[该页面](https://faucet.goerli.mudit.blog/)来领取测试的以太币。

这里需要你通过在Twitter或者Facebook上发帖，其中粘贴你的以太币地址，复制帖子链接来领取。所以没有Twitter或者Facebook账号的需要先注册一个。网站会自动检测根据你账号注册的时长来分发可领取的测试币数量。其中9天以上的一次可领取87.5个测试的以太币，这足够你进行一次抵押操作。

当然，如果你没有以太坊钱包，需要事先安装一个，推荐用[metamask](https://metamask.io/)插件钱包，这个也方便你后续通过[launchpad](https://pyrmont.launchpad.ethereum.org/)进行抵押操作。

## 运行验证程序的步骤

1. 同步以太坊1.0的网络。

   因为参与验证者必须首先在1.0网络上抵押32个以太币，所以你同时需要同步1.0的网络，以便2.0的验证程序需要获取1.0网络的最新数据，将新的验证者加入进来。也就是说，运行验证程序意味着你同时需要运行一个1.0网络的节点。

   除了本地运行1.0网络的节点，你也可以使用第三方提供云服务，例如：Infura提供的数据源。那么，这一步就可以省略，只需要在后面启动 `teku` 的时候将`eth1-endpoint`配置成Infura的即可。详细url可以去[Infura的网站](https://infura.io/)获取。

   当然，我们建议你本地运行1.0网络的节点，这样可以使以太坊的网络更加的去中心化。

2. 生成验证者Key并发送以太币到抵押合约

   在这一步，你需要根据[launchpad](https://pyrmont.launchpad.ethereum.org/)的提示，生成运行验证程序所需要的key等相关文件，并发送32个以太币到抵押合约来完成抵押。我们会在[如何进行抵押](how-deposit.html)这一节详细介绍这一操作过程。

3. 为每一个验证者key创建密码文件

   每一个验证者key文件，都需要有一个和key文件同名的`.txt`文件，其中包含解密key的密码。这样`teku`才可以正确识别和加载。

4. 启动验证程序

   使用类似如下的命令启动Teku，关于命令参数详细我们会在[启动验证程序](start-validator.html)这一节详细阐述。

   ``` bash
   teku --network=pyrmont --eth1-endpoint=https://goerli.infura.io/v3/ce064631a5954a08b8bdfdb6d082a6ec  --validator-keys=C:/Users/Administrator/Downloads/eth2deposit-cli-ed5a6d3-windows-amd64/validator_keys/keystore-m_12381_3600_0_0_0-1606463886.json;C:/Users/Administrator/Downloads/eth2deposit-cli-ed5a6d3-windows-amd64/validator_keys/keystore-m_12381_3600_0_0_0-1606463886.txt  --rest-api-enabled=true --rest-api-docs-enabled=true  --metrics-enabled
   ```

当验证程序激活后，你就可以访问[信标链浏览器](https://pyrmont.beaconcha.in)，查看到你的验证节点信息。

