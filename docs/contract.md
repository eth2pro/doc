# Contract

### 常用方法

合约里面如何判断address是外部账户还是合约账户的方法：

```javascript

pragma solidity ^0.8.7;

contract IsContract {
    function _isContract(address _address) public view returns (bool) {
        uint256 _size;
        // solium-disable-next-line security/no-inline-assembly
        assembly {
            _size := extcodesize(_address)
        }
        return _size > 0;
    }
}


```